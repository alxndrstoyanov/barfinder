package com.techhuddletask.astoyanov.barfinder.cmn;

import android.location.Location;

/**
 * Created by alexander on 12/20/15.
 */
public class Geometry {
    private BarLocation location;

    public BarLocation getLocation() {
        return location;
    }

    public void setLocation(BarLocation location) {
        this.location = location;
    }
}
