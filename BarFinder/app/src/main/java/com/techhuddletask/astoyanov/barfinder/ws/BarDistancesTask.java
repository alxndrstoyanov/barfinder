package com.techhuddletask.astoyanov.barfinder.ws;

import android.os.AsyncTask;

import com.techhuddletask.astoyanov.barfinder.cmn.request.BarDistancesRequest;
import com.techhuddletask.astoyanov.barfinder.cmn.response.BarDistancesResponse;
import com.techhuddletask.astoyanov.barfinder.data.BarDistancesProvider;
import com.techhuddletask.astoyanov.barfinder.data.DataProvider;
import com.techhuddletask.astoyanov.barfinder.util.Constants;

/**
 * Created by astoyanov on 15-12-23.
 */
public class BarDistancesTask extends AsyncTask<String, Void, BarDistancesProvider> {
    private String origins;
    private String key;
    private String destinations;

    private DataProvider provider;
    private AsyncTaskReturnListener listener;

    public BarDistancesTask(AsyncTaskReturnListener listener) {
        this.listener = listener;
    }

    @Override
    protected BarDistancesProvider doInBackground(String... params) {
        BarDistancesRequest requestBarDistances = new BarDistancesRequest();
        key = params[0];
        origins = params[1];
        destinations = params[2];
        requestBarDistances.setKey(key);
        requestBarDistances.setDestinations(destinations);
        requestBarDistances.setOrigins(origins);
        provider = new BarDistancesProvider(requestBarDistances);
        return (BarDistancesProvider) provider;
    }

    @Override
    protected void onPostExecute(BarDistancesProvider providerBarDistances) {
        super.onPostExecute(providerBarDistances);
        listener.onResult(providerBarDistances);
        if (!providerBarDistances.hasError()) {
            listener.onSuccess();
            listener.onResult(providerBarDistances);
        } else {
            listener.onError(providerBarDistances.getErrorMessage());
        }
    }
}