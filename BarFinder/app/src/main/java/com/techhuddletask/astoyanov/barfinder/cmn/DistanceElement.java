package com.techhuddletask.astoyanov.barfinder.cmn;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by astoyanov on 15-12-23.
 */
public class DistanceElement {
    private Distance distance;

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

}
