package com.techhuddletask.astoyanov.barfinder.cmn;

/**
 * Created by alexander on 12/20/15.
 */
public class BarLocation {
    private String lat;
    private String lng;

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

}
