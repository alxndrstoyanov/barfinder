package com.techhuddletask.astoyanov.barfinder.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.techhuddletask.astoyanov.barfinder.R;
import com.techhuddletask.astoyanov.barfinder.cmn.Bar;
import com.techhuddletask.astoyanov.barfinder.cmn.DistanceElement;
import com.techhuddletask.astoyanov.barfinder.cmn.DistancesRow;
import com.techhuddletask.astoyanov.barfinder.cmn.response.BarDistancesResponse;
import com.techhuddletask.astoyanov.barfinder.cmn.response.NearbyPlacesResponse;
import com.techhuddletask.astoyanov.barfinder.data.BarDistancesProvider;
import com.techhuddletask.astoyanov.barfinder.data.DataProvider;
import com.techhuddletask.astoyanov.barfinder.data.NearbyPlacesProvider;
import com.techhuddletask.astoyanov.barfinder.util.BarFinderDatabaseHelper;
import com.techhuddletask.astoyanov.barfinder.util.Constants;
import com.techhuddletask.astoyanov.barfinder.util.adapters.BarsAdapter;
import com.techhuddletask.astoyanov.barfinder.ws.AsyncTaskReturnListener;
import com.techhuddletask.astoyanov.barfinder.ws.BarDistancesTask;
import com.techhuddletask.astoyanov.barfinder.ws.NearbyPlacesTask;

import java.util.ArrayList;
import java.util.List;

public class BarsListFragment extends Fragment implements AsyncTaskReturnListener {
    private ListView mBarsLv;
    private List<Bar> mBars;
    private SharedPreferences mSharedPreferences;
    private BarsAdapter mBarsAdapter;
    private LatLng mCurrentPlaceLatLng;
    private BarFinderDatabaseHelper mDbHelper;

    public static BarsListFragment newInstance() {
        BarsListFragment fragment = new BarsListFragment();
        return fragment;
    }

    public BarsListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bars_list, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        initFields();
        initViews(view);
        initListeners();
    }

    private void initListeners() {
        mBarsLv.setOnScrollListener(new EndlessScrollListener());
        mBarsLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bar selectedBar = mBars.get(position);
                Uri gmmIntentUri = Uri.parse("geo:" + selectedBar.getLat() + ","
                        + selectedBar.getLng() + "?q=" + Uri.encode(selectedBar.getName()));
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(mapIntent);
                }
            }
        });
    }

    private void initFields() {
        mDbHelper = BarFinderDatabaseHelper.getInstance(getActivity());
        mSharedPreferences = getActivity().getSharedPreferences(Constants.PREFERENCES, Context.MODE_PRIVATE);
        mBars = mDbHelper.retrieveBars();
        mBarsAdapter = new BarsAdapter(getActivity(), (ArrayList<Bar>) mBars);
    }

    private void initViews(View view) {
        mBarsLv = (ListView) view.findViewById(R.id.list_bars);
        mBarsLv.setAdapter(mBarsAdapter);
    }

    public void requestBars(LatLng currentPlace) {
        //If there is a location - clear all bars because the location could have changed and request new bars.
        mDbHelper.clearBars();
        mBars.clear();
        mBarsAdapter.notifyDataSetChanged();
        mSharedPreferences.edit().putString(Constants.NEXT_PAGE_TOKEN, null).commit();
        mCurrentPlaceLatLng = currentPlace;
        NearbyPlacesTask taskNearbyPlaces = new NearbyPlacesTask(this);
        String lngLat = String.valueOf(mCurrentPlaceLatLng.latitude) + ", " +
                String.valueOf(mCurrentPlaceLatLng.longitude);
        taskNearbyPlaces.execute(lngLat, Constants.RANK_BY, Constants.PLACE_TYPES, Constants.WEB_SERVICE_API_KEY, null);
    }

    @Override
    public void onSuccess() {

    }

    @Override
    public void onError(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResult(DataProvider provider) {
        if (getActivity() != null) {
            mDbHelper = BarFinderDatabaseHelper.getInstance(getActivity());
            if (provider instanceof NearbyPlacesProvider) {
                mBars.addAll(((NearbyPlacesResponse) provider.getResponse()).getBars());
                mBarsAdapter.notifyDataSetChanged();
                mDbHelper.insertOrReplaceBars(mBars);
                mSharedPreferences.edit().putString(Constants.NEXT_PAGE_TOKEN,
                        ((NearbyPlacesResponse) provider.getResponse()).getNextPageToken()).commit();
                requestBarDistances();
            } else if (provider instanceof BarDistancesProvider) {
                int count = 0;
                BarDistancesResponse barDistancesResponse = (BarDistancesResponse) provider.getResponse();
                for (DistancesRow row : barDistancesResponse.getRows()) {
                    for (DistanceElement element : row.getElements()) {
                        mBars.get(count++).setDistance(element.getDistance().getText());
                    }
                }
                mBarsAdapter.notifyDataSetChanged();
                mDbHelper.insertOrReplaceBars(mBars);
            }
        }
    }

    private void requestBarDistances() {
        StringBuilder stringBuilder = new StringBuilder();
        String prefix = "";
        for (Bar bar : mBars) {
            stringBuilder.append(prefix);
            prefix = "|";
            stringBuilder.append(bar.getGeometry().getLocation().getLat() + " ,"
                    + bar.getGeometry().getLocation().getLng());
        }
        String destinations = stringBuilder.toString();
        BarDistancesTask barDistancesTask = new BarDistancesTask(this);
        barDistancesTask.execute(Constants.WEB_SERVICE_API_KEY, mCurrentPlaceLatLng.latitude + " ,"
                + mCurrentPlaceLatLng.longitude, destinations);
    }

    public class EndlessScrollListener implements AbsListView.OnScrollListener {

        private int visibleThreshold = 5;
        private int currentPage = 0;
        private int previousTotal = 0;
        private boolean loading = true;

        public EndlessScrollListener() {
        }

        public EndlessScrollListener(int visibleThreshold) {
            this.visibleThreshold = visibleThreshold;
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem,
                             int visibleItemCount, int totalItemCount) {
            if (loading) {
                if (totalItemCount > previousTotal) {
                    loading = false;
                    previousTotal = totalItemCount;
                    currentPage++;
                }
            }
            if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                String nextPageToken = mSharedPreferences.getString(Constants.NEXT_PAGE_TOKEN, null);
                if (nextPageToken != null && mCurrentPlaceLatLng != null) {
                    NearbyPlacesTask taskNearbyPlaces = new NearbyPlacesTask(BarsListFragment.this);
                    String lngLat = String.valueOf(mCurrentPlaceLatLng.latitude) + ", " +
                            String.valueOf(mCurrentPlaceLatLng.longitude);
                    taskNearbyPlaces.execute(lngLat, Constants.RANK_BY, Constants.PLACE_TYPES, Constants.WEB_SERVICE_API_KEY, nextPageToken);
                    loading = true;
                }
            }
        }

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }
    }

    public void setLocation(Location location) {
        if (location != null) {
            this.mCurrentPlaceLatLng = new LatLng(location.getLatitude(), location.getLongitude());
        }
    }
}
