package com.techhuddletask.astoyanov.barfinder.util;

/**
 * Created by alexander on 12/20/15.
 */
public class Constants {
    public static final String GOOGLE_MAPS_API_PATH = "https://maps.googleapis.com/";
    public static final String NEARBY_PLACES_PATH = "/maps/api/place/nearbysearch/json";
    public static final String BAR_DISTANCES_PATH = "/maps/api/distancematrix/json";
    public static final String PLACE_TYPES = "bar";
    public static final String OK = "OK";
    public static final String RANK_BY = "distance";
    public static final String WEB_SERVICE_API_KEY = "AIzaSyB2FwbmuO-PXJNkNjHmLxkEzOJMELb-BOA";
    public static final int PERMISSION_USE_LOCATION = 1;
    public static final String PREFERENCES = "BarFinderPrivatePrefs";
    public static final int MAIN_ACTIVITY_TAB_COUNT = 2;
    public static final String LOCATION_TAG = "LocationService";

    public static final String NEXT_PAGE_TOKEN = "nextPageToken";
    public static final float MAP_ZOOM_LEVEL = 13;
    public static final int BARS_LIST_FRAGMENT_POSITION = 0;
    public static final int BARS_MAP_FRAGMENT_POSITION = 1;
    public static final String LOCATION_KEY = "LocationState";
    public static final String IS_REQUESTING_LOCATION_UPDATES_KEY = "LocationBool";
    public static final String IS_SHOWING_LOCATION_REQUEST_DIALOG = "LocationRequestDialogBool";
}
