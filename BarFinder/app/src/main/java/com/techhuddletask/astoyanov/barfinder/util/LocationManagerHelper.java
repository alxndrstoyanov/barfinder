package com.techhuddletask.astoyanov.barfinder.util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;

import com.techhuddletask.astoyanov.barfinder.R;

/**
 * Created by alexander on 12/28/15.
 */
public class LocationManagerHelper {
    private static LocationManager sLocationManager;
    private static AlertDialog sAlert;

    public static boolean isLocationServiceAvailable(Context context) {
        sLocationManager = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        boolean gpsIsEnabled = sLocationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        return gpsIsEnabled;
    }

    public static void showLocationServiceDialog(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setMessage(
                activity.getString(R.string.msg_turn_on_gps))
                .setTitle(activity.getResources().getString(R.string.app_name))
                .setCancelable(false)
                .setPositiveButton(activity.getString(R.string.settings),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent(
                                        Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                activity.startActivity(intent);
                                sAlert.dismiss();
                            }
                        })
                .setNegativeButton(activity.getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                sAlert.dismiss();
                            }
                        });
        sAlert = builder.create();
        sAlert.show();
    }
}
