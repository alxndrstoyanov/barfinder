package com.techhuddletask.astoyanov.barfinder.data;

import com.techhuddletask.astoyanov.barfinder.cmn.response.BarFinderResponse;

/**
 * Created by alexander on 12/20/15.
 */

public interface DataProvider {

    public BarFinderResponse getResponse();

    public String getErrorMessage();

    public boolean hasError();

}
