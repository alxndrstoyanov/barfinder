package com.techhuddletask.astoyanov.barfinder.ui;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.techhuddletask.astoyanov.barfinder.R;
import com.techhuddletask.astoyanov.barfinder.cmn.Bar;
import com.techhuddletask.astoyanov.barfinder.ui.fragments.BarsListFragment;
import com.techhuddletask.astoyanov.barfinder.ui.fragments.BarsMapFragment;
import com.techhuddletask.astoyanov.barfinder.util.BarFinderDatabaseHelper;
import com.techhuddletask.astoyanov.barfinder.util.Constants;
import com.techhuddletask.astoyanov.barfinder.util.LocationManagerHelper;
import com.techhuddletask.astoyanov.barfinder.util.PermissionsManager;

import java.util.List;

public class MainActivity extends ActionBarActivity implements ActionBar.TabListener, OnMapReadyCallback
        , GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private AppSectionsPagerAdapter mAppSectionsPagerAdapter;
    private ViewPager mViewPager;
    private ActionBar mActionBar;
    private TextView mTxtPermissionDisabled;
    private BarFinderDatabaseHelper mDbHelper;
    private GoogleApiClient mGoogleApiClient;
    private GoogleMap mGoogleMap;
    private boolean mIsRequestingLocationUpdates;
    private LocationRequest mLocationRequest;
    private Location mCurrentLocation;
    private boolean mIsShowingLocationRequestDialog;


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init(savedInstanceState);
    }

    private void init(Bundle savedInstanceState) {
        initFields();
        initViews();
        setUpViews();
        initListeners();
        createLocationRequest();
        updateValuesFromBundle(savedInstanceState);
    }

    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.keySet().contains(Constants.IS_REQUESTING_LOCATION_UPDATES_KEY)) {
                mIsRequestingLocationUpdates = savedInstanceState.getBoolean(
                        Constants.IS_REQUESTING_LOCATION_UPDATES_KEY);
            }
            if (savedInstanceState.keySet().contains(Constants.LOCATION_KEY)) {
                mCurrentLocation = savedInstanceState.getParcelable(Constants.LOCATION_KEY);
                BarsListFragment barsListFragment =
                        (BarsListFragment) getSupportFragmentManager().
                                findFragmentByTag(getFragmentTag(mViewPager.getId(),
                                        Constants.BARS_LIST_FRAGMENT_POSITION));
                barsListFragment.setLocation(mCurrentLocation);
            }
            if (savedInstanceState.keySet().contains(Constants.IS_SHOWING_LOCATION_REQUEST_DIALOG)) {
                mIsShowingLocationRequestDialog = savedInstanceState.getBoolean(
                        Constants.IS_SHOWING_LOCATION_REQUEST_DIALOG);
            }
        }
    }

    private void setUpViews() {
        mActionBar.setHomeButtonEnabled(false);
        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        mViewPager.setAdapter(mAppSectionsPagerAdapter);
        for (int i = 0; i < mAppSectionsPagerAdapter.getCount(); i++) {
            mActionBar.addTab(
                    mActionBar.newTab()
                            .setText(mAppSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }
        mAppSectionsPagerAdapter.notifyDataSetChanged();
    }

    private void initListeners() {
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                mActionBar.setSelectedNavigationItem(position);
            }
        });
    }

    private void initFields() {
        mDbHelper = BarFinderDatabaseHelper.getInstance(this);
        mLocationRequest = new LocationRequest();
        mIsRequestingLocationUpdates = false;
        mIsShowingLocationRequestDialog = false;
        mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(getSupportFragmentManager(), getApplicationContext());
        mGoogleApiClient = new GoogleApiClient.Builder(this).
                addApi(Places.GEO_DATA_API).
                addApi(Places.PLACE_DETECTION_API).
                addApi(LocationServices.API).
                addConnectionCallbacks(this).
                addOnConnectionFailedListener(this).
                build();
    }

    private void initViews() {
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mActionBar = getSupportActionBar();
        mTxtPermissionDisabled = (TextView) findViewById(R.id.txt_permission_disabled);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(Constants.IS_REQUESTING_LOCATION_UPDATES_KEY,
                mIsRequestingLocationUpdates);
        savedInstanceState.putBoolean(Constants.IS_SHOWING_LOCATION_REQUEST_DIALOG,
                mIsShowingLocationRequestDialog);
        savedInstanceState.putParcelable(Constants.LOCATION_KEY, mCurrentLocation);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {
        mViewPager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (mCurrentLocation != null) {
            LatLng currentPlaceLatLng = new LatLng(mCurrentLocation.getLatitude(),
                    mCurrentLocation.getLongitude());
            this.mGoogleMap = googleMap;
            List<Bar> bars = mDbHelper.retrieveBars();
            for (Bar bar : bars) {
                googleMap.addMarker(new MarkerOptions().
                        position(new LatLng(Double.parseDouble(bar.getLat()),
                                Double.parseDouble(bar.getLng()))).
                        title(bar.getName()).
                        snippet(bar.getDistance()));
                CameraUpdate center =
                        CameraUpdateFactory.newLatLng(currentPlaceLatLng);
                CameraUpdate zoom = CameraUpdateFactory.zoomTo(Constants.MAP_ZOOM_LEVEL);
                if (googleMap != null) {
                    googleMap.moveCamera(center);
                    googleMap.animateCamera(zoom);
                }
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(Constants.LOCATION_TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(Constants.LOCATION_TAG, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }

    public static class AppSectionsPagerAdapter extends FragmentPagerAdapter {
        private Context context;

        public AppSectionsPagerAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context = context;
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case Constants.BARS_LIST_FRAGMENT_POSITION:
                    BarsListFragment barsListFragment = BarsListFragment.newInstance();
                    return barsListFragment;
                case Constants.BARS_MAP_FRAGMENT_POSITION:
                    BarsMapFragment mapFragment = BarsMapFragment.newInstance();
                    return mapFragment;
                default:
                    return BarsListFragment.newInstance();
            }
        }

        @Override
        public int getCount() {
            return Constants.MAIN_ACTIVITY_TAB_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case Constants.BARS_LIST_FRAGMENT_POSITION:
                    return context.getString(R.string.bars_list_tab_title);
                case Constants.BARS_MAP_FRAGMENT_POSITION:
                    return context.getString(R.string.map_bars_tab_title);
            }

            return super.getPageTitle(position);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Constants.PERMISSION_USE_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mTxtPermissionDisabled.setVisibility(View.GONE);
                } else {
                    mTxtPermissionDisabled.setVisibility(View.VISIBLE);
                }
                return;
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private String getFragmentTag(int viewPagerId, int fragmentPosition) {
        return "android:switcher:" + viewPagerId + ":" + fragmentPosition;
    }

    public void getLocationRequestBars(Location location) {
        LatLng currentPlaceLatLng = new LatLng(location.getLatitude(), location.getLongitude());
        BarsListFragment barsListFragment =
                (BarsListFragment) getSupportFragmentManager().
                        findFragmentByTag(getFragmentTag(mViewPager.getId(),
                                Constants.BARS_LIST_FRAGMENT_POSITION));
        barsListFragment.requestBars(currentPlaceLatLng);
    }


    protected void createLocationRequest() {
        mLocationRequest.setInterval(600000);
        mLocationRequest.setFastestInterval(300000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        startLocationUpdates();
    }

    protected void startLocationUpdates() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            PermissionsManager.showPermissionDialog(this, Manifest.permission.ACCESS_FINE_LOCATION,
                    Constants.PERMISSION_USE_LOCATION);
            mTxtPermissionDisabled.setVisibility(View.VISIBLE);
        } else {
            if (!mIsRequestingLocationUpdates) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,
                        (LocationListener) this);
                mIsRequestingLocationUpdates = true;
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        BarsListFragment barsListFragment =
                (BarsListFragment) getSupportFragmentManager().
                        findFragmentByTag(getFragmentTag(mViewPager.getId(),
                                Constants.BARS_LIST_FRAGMENT_POSITION));
        barsListFragment.setLocation(location);
        getLocationRequestBars(location);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!LocationManagerHelper.isLocationServiceAvailable(this)) {
            if (!mIsShowingLocationRequestDialog) {
                LocationManagerHelper.showLocationServiceDialog(this);
                mIsShowingLocationRequestDialog = true;
            }
        } else if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        } else {
            mGoogleApiClient.connect();
        }
    }
}
