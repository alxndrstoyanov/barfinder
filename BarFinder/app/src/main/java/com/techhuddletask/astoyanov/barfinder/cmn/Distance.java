package com.techhuddletask.astoyanov.barfinder.cmn;

/**
 * Created by astoyanov on 15-12-23.
 */
public class Distance {
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
