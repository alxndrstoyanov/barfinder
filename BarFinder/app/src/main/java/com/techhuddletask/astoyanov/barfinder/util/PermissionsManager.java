package com.techhuddletask.astoyanov.barfinder.util;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.techhuddletask.astoyanov.barfinder.R;

/**
 * Created by alexander on 12/20/15.
 */
public class PermissionsManager {
    private static final String TAG = "PermissionManager";
    
    public static void showPermissionDialog(Activity activity, String permission, int permissionCode) {
        if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                Log.i(TAG,
                        activity.getString(R.string.permission_not_granted));
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{permission},
                        permissionCode);
            }
        }
    }
}
