package com.techhuddletask.astoyanov.barfinder.cmn;

import java.util.List;

/**
 * Created by astoyanov on 15-12-23.
 */
public class DistancesRow {
    public List<DistanceElement> getElements() {
        return elements;
    }

    public void setElements(List<DistanceElement> elements) {
        this.elements = elements;
    }

    private List<DistanceElement> elements;

}
