package com.techhuddletask.astoyanov.barfinder.ws;

import android.os.AsyncTask;

import com.techhuddletask.astoyanov.barfinder.cmn.request.NearbyPlacesRequest;
import com.techhuddletask.astoyanov.barfinder.cmn.response.NearbyPlacesResponse;
import com.techhuddletask.astoyanov.barfinder.data.DataProvider;
import com.techhuddletask.astoyanov.barfinder.data.NearbyPlacesProvider;
import com.techhuddletask.astoyanov.barfinder.util.Constants;

/**
 * Created by alexander on 12/20/15.
 */
public class NearbyPlacesTask extends AsyncTask<String, Void, NearbyPlacesProvider> {
    private String location;
    private String rankBy;
    private String types;
    private String key;
    private String nextPageToken;

    private DataProvider provider;
    private AsyncTaskReturnListener listener;

    public NearbyPlacesTask(AsyncTaskReturnListener listener) {
        this.listener = listener;
    }

    @Override
    protected NearbyPlacesProvider doInBackground(String... params) {
        NearbyPlacesRequest requestNearbyPlaces = new NearbyPlacesRequest();
        location = params[0];
        rankBy = params[1];
        types = params[2];
        key = params[3];
        nextPageToken = params[4];
        requestNearbyPlaces.setKey(key);
        requestNearbyPlaces.setRankBy(rankBy);
        requestNearbyPlaces.setType(types);
        requestNearbyPlaces.setLocation(location);
        requestNearbyPlaces.setPagetoken(nextPageToken);
        provider = new NearbyPlacesProvider(requestNearbyPlaces);
        return (NearbyPlacesProvider) provider;
    }

    @Override
    protected void onPostExecute(NearbyPlacesProvider providerNearbyPlaces) {
        super.onPostExecute(providerNearbyPlaces);
        if (!providerNearbyPlaces.hasError()) {
            listener.onSuccess();
            listener.onResult(providerNearbyPlaces);
        } else {
            listener.onError(providerNearbyPlaces.getErrorMessage());
        }
    }
}
