package com.techhuddletask.astoyanov.barfinder.cmn.response;

import com.techhuddletask.astoyanov.barfinder.cmn.DistancesRow;

import java.util.List;

/**
 * Created by astoyanov on 15-12-23.
 */
public class BarDistancesResponse extends BarFinderResponse{
    private List<DistancesRow> rows;

    public List<DistancesRow> getRows() {
        return rows;
    }

    public void setRows(List<DistancesRow> rows) {
        this.rows = rows;
    }

}
