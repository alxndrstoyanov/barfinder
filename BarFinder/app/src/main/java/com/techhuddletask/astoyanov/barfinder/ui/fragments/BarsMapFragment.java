package com.techhuddletask.astoyanov.barfinder.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

public class BarsMapFragment extends SupportMapFragment {
    private Activity mActivity;
    public BarsMapFragment() {
    }

    public static BarsMapFragment newInstance() {
        BarsMapFragment fragment = new BarsMapFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMapReadyCallback) {
            mActivity = (Activity) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnMapReadyCallback");
        }
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            getMapAsync((OnMapReadyCallback) mActivity);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
