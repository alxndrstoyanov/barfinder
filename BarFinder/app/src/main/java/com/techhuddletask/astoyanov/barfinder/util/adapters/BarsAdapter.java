package com.techhuddletask.astoyanov.barfinder.util.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.techhuddletask.astoyanov.barfinder.R;
import com.techhuddletask.astoyanov.barfinder.cmn.Bar;

import java.util.ArrayList;

/**
 * Created by alexander on 12/20/15.
 */
public class BarsAdapter extends ArrayAdapter<Bar> {
    public BarsAdapter(Context context, ArrayList<Bar> bars) {
        super(context, 0, bars);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Bar bar = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_bars_list, parent, false);
        }
        TextView txtBarName = (TextView)convertView.findViewById(R.id.txt_bar_name);
        TextView txtDistance = (TextView)convertView.findViewById(R.id.txt_distance);
        txtBarName.setText(bar.getName());
        txtDistance.setText(bar.getDistance());
        return convertView;
    }
}
