package com.techhuddletask.astoyanov.barfinder.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.techhuddletask.astoyanov.barfinder.cmn.Bar;
import com.techhuddletask.astoyanov.barfinder.cmn.dao.BarDao;
import com.techhuddletask.astoyanov.barfinder.cmn.dao.DaoMaster;
import com.techhuddletask.astoyanov.barfinder.cmn.dao.DaoSession;

import java.util.List;

/**
 * Created by alexander on 12/25/15.
 */
public class BarFinderDatabaseHelper {
    private static DaoMaster sDaoMaster;
    private static DaoSession sDaoSession;
    private static SQLiteDatabase sDb;
    private static BarFinderDatabaseHelper sBarFinderDatabaseHelper;
    private static BarDao sBarDao;

    public static BarFinderDatabaseHelper getInstance(Context context) {
        if (sBarFinderDatabaseHelper == null) {
            sBarFinderDatabaseHelper = new BarFinderDatabaseHelper(context);
        }
        return sBarFinderDatabaseHelper;
    }

    private BarFinderDatabaseHelper(Context context) {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, "bar-finder-db", null);
        sDb = helper.getWritableDatabase();
        sDaoMaster = new DaoMaster(sDb);
        sDaoSession = sDaoMaster.newSession();
        sBarDao = sDaoSession.getBarDao();
    }

    private BarFinderDatabaseHelper() {
    }

    public static void insertOrReplaceBar(Bar bar) {
        if (bar != null) {
            bar.setLng(bar.getGeometry().getLocation().getLng());
            bar.setLat(bar.getGeometry().getLocation().getLat());
            sBarDao.insertOrReplace(bar);
        }
    }

    public void insertOrReplaceBars(List<Bar> bars) {
        for (Bar bar : bars) {
            insertOrReplaceBar(bar);
        }
    }

    public List<Bar> retrieveBars() {
        List<Bar> bars = sBarDao.loadAll();
        return bars;
    }

    public void clearBars() {
        sBarDao.deleteAll();
    }
}
