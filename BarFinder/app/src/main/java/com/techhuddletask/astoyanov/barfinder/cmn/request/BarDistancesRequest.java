package com.techhuddletask.astoyanov.barfinder.cmn.request;

/**
 * Created by astoyanov on 15-12-23.
 */
public class BarDistancesRequest extends BarFinderRequest {
    private String origins;
    private String destinations;

    public String getOrigins() {
        return origins;
    }

    public void setOrigins(String origins) {
        this.origins = origins;
    }

    public String getDestinations() {
        return destinations;
    }

    public void setDestinations(String destinations) {
        this.destinations = destinations;
    }

}