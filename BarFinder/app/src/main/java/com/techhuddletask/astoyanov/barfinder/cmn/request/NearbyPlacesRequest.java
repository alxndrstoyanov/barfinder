package com.techhuddletask.astoyanov.barfinder.cmn.request;

/**
 * Created by alexander on 12/20/15.
 */
public class NearbyPlacesRequest extends BarFinderRequest {
    private String location;
    private String rankBy;
    private String type;
    private String pagetoken;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRankBy() {
        return rankBy;
    }

    public void setRankBy(String rankBy) {
        this.rankBy = rankBy;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPagetoken() {
        return pagetoken;
    }

    public void setPagetoken(String pageToken) {
        this.pagetoken = pageToken;
    }
}
