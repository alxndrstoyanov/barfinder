package com.techhuddletask.astoyanov.barfinder.cmn.response;

import com.google.gson.annotations.SerializedName;
import com.techhuddletask.astoyanov.barfinder.cmn.Bar;

import java.util.List;

/**
 * Created by alexander on 12/20/15.
 */
public class NearbyPlacesResponse extends BarFinderResponse {
    @SerializedName("results")
    private List<Bar> bars;
    private String nextPageToken;

    public List<Bar> getBars() {
        return bars;
    }

    public void setBars(List<Bar> bars) {
        this.bars = bars;
    }

    public String getNextPageToken() {
        return nextPageToken;
    }

    public void setNextPageToken(String nextPageToken) {
        this.nextPageToken = nextPageToken;
    }
}
