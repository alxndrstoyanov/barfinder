package com.techhuddletask.astoyanov.barfinder.cmn.response;

/**
 * Created by alexander on 12/20/15.
 * Base class for responses.
 */
public class BarFinderResponse {
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
