package com.techhuddletask.astoyanov.barfinder.data;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techhuddletask.astoyanov.barfinder.cmn.request.NearbyPlacesRequest;
import com.techhuddletask.astoyanov.barfinder.cmn.response.BarFinderResponse;
import com.techhuddletask.astoyanov.barfinder.cmn.response.NearbyPlacesResponse;
import com.techhuddletask.astoyanov.barfinder.util.Constants;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by alexander on 12/20/15.
 */
public class NearbyPlacesProvider implements DataProvider {
    private NearbyPlacesResponse mResponse;

    private Gson mGson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).
            create();
    private RestAdapter mRestAdapter = new RestAdapter.Builder().setEndpoint(Constants.GOOGLE_MAPS_API_PATH).
            setConverter(new GsonConverter(mGson)).build();

    public NearbyPlacesProvider(NearbyPlacesRequest request) {
        this.mResponse = service.requestNearbyPlaces(request.getKey(),
                request.getLocation(), request.getRankBy(), request.getType(), request.getPagetoken());
    }

    NearbyPlacesService service = mRestAdapter.create(NearbyPlacesService.class);

    @Override
    public BarFinderResponse getResponse() {
        return mResponse;
    }

    @Override
    public String getErrorMessage() {
        return mResponse.getStatus();
    }

    @Override
    public boolean hasError() {
        return !(mResponse != null &&
                mResponse.getStatus() != null &&
                mResponse.getStatus().equals(Constants.OK));
    }

    public interface NearbyPlacesService {
        @GET(Constants.NEARBY_PLACES_PATH)
        NearbyPlacesResponse requestNearbyPlaces(@Query("key") String key,
                                                 @Query("location") String location,
                                                 @Query("rankby") String rankBy,
                                                 @Query("types") String types,
                                                 @Query("pagetoken") String pagetoken);
    }

}
