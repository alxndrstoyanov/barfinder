package com.techhuddletask.astoyanov.barfinder.cmn.request;

/**
 * Created by alexander on 12/20/15.
 * Base class for responses.
 */
public class BarFinderRequest {
    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
