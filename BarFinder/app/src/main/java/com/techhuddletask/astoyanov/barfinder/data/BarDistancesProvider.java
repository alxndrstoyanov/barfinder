package com.techhuddletask.astoyanov.barfinder.data;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techhuddletask.astoyanov.barfinder.cmn.request.BarDistancesRequest;
import com.techhuddletask.astoyanov.barfinder.cmn.response.BarDistancesResponse;
import com.techhuddletask.astoyanov.barfinder.cmn.response.BarFinderResponse;
import com.techhuddletask.astoyanov.barfinder.util.Constants;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by astoyanov on 15-12-23.
 */
public class BarDistancesProvider implements DataProvider {
    private BarDistancesResponse mResponse;

    private Gson mGson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).
            create();
    private RestAdapter mRestAdapter = new RestAdapter.Builder().setEndpoint(Constants.GOOGLE_MAPS_API_PATH).
            setConverter(new GsonConverter(mGson)).build();

    public BarDistancesProvider(BarDistancesRequest request) {
        this.mResponse = service.requestBarDistances(request.getKey(),
                request.getOrigins(), request.getDestinations());
    }

    BarDistancesService service = mRestAdapter.create(BarDistancesService.class);

    @Override
    public BarFinderResponse getResponse() {
        return mResponse;
    }

    @Override
    public String getErrorMessage() {
        return mResponse.getStatus();
    }

    @Override
    public boolean hasError() {
        return !(mResponse != null &&
                mResponse.getStatus() != null &&
                mResponse.getStatus().equals(Constants.OK));
    }

    public interface BarDistancesService {
        @GET(Constants.BAR_DISTANCES_PATH)
        BarDistancesResponse requestBarDistances(@Query("key") String key,
                                                 @Query("origins") String origins,
                                                 @Query("destinations") String destinations);
    }
}