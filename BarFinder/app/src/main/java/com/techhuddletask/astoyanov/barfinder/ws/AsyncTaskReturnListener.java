package com.techhuddletask.astoyanov.barfinder.ws;

import com.techhuddletask.astoyanov.barfinder.data.DataProvider;

/**
 * Created by alexander on 12/20/15.
 */
public interface AsyncTaskReturnListener {
    public void onSuccess();

    public void onError(String error);

    public void onResult(DataProvider provider);
}
