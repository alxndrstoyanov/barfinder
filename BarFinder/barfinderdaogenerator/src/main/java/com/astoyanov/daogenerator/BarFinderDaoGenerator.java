package com.astoyanov.daogenerator;


import java.io.IOException;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;
import de.greenrobot.daogenerator.ToMany;

/**
 * Created by alexander on 12/25/15.
 */
public class BarFinderDaoGenerator {
    private static Entity bar;
    private static Entity geometry;
    private static Entity barLocation;
    private static Entity distance;
    private static Entity distanceElement;
    private static Entity distanceRow;


    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(1000, "com.techhuddletask.astoyanov.barfinder.cmn");
        schema.setDefaultJavaPackageDao("com.techhuddletask.astoyanov.barfinder.cmn.dao");
        addBar(schema);
        new DaoGenerator().generateAll(schema, "app/src/main/java");
    }

    private static void addDistanceRow(Schema schema) {
        distanceRow = schema.addEntity("DistanceRow");
        distanceRow.addStringProperty("id").primaryKey();
    }

    private static void addDistanceElement(Schema schema) {
        distanceElement = schema.addEntity("DistanceElement");
        distanceElement.addStringProperty("id").primaryKey();
        Property distanceId = distanceElement.addStringProperty("distanceId").getProperty();
        distanceElement.addToOne(distance, distanceId);

        Property distanceRowId = distanceElement.addStringProperty("distanceRowId").getProperty();
        distanceElement.addToOne(distanceRow, distanceRowId);

        ToMany rowToElements = distanceRow.addToMany(distanceElement, distanceRowId);
        rowToElements.setName("elements");
    }

    private static void addDistance(Schema schema) {
        distance = schema.addEntity("Distance");
        distance.addStringProperty("id").primaryKey();
        distance.addStringProperty("text");
    }

    private static void addBarLocation(Schema schema) {
        barLocation = schema.addEntity("BarLocation");
        barLocation.addStringProperty("id").primaryKey();
        barLocation.addStringProperty("lat");
        barLocation.addStringProperty("lng");
    }

    private static void addGeometry(Schema schema) {
        geometry = schema.addEntity("Geometry");
        geometry.addStringProperty("id").primaryKey();
        Property barLocationId = geometry.addStringProperty("barLocationId").getProperty();
        geometry.addToOne(barLocation, barLocationId);

    }

    private static void addBar(Schema schema) {
        bar = schema.addEntity("Bar");
        bar.addStringProperty("id").primaryKey();
        bar.addStringProperty("placeId");
        bar.addStringProperty("distance");
        bar.addStringProperty("name");
        bar.addStringProperty("lat");
        bar.addStringProperty("lng");
    }
}
